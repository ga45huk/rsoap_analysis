loadfolder = 'H:\2_basic_analysis_values_mat\ml_models\pca\';

model_name = 'all_without_cycles_analysis_without_depth';

loadname = strcat (loadfolder,model_name,'_pca.mat');
load(loadname, 'params_values_normalized', 'factors_to_include','coeff','score','latent','tsquared','explained','mu');

loadfolder = 'H:\2_basic_analysis_values_mat\features_values_training\';

params_values = [];
data_labels = [];

sample_names = ["1766_4t1_RePCM_Ear30_day_1","1766_4t1_RePCM_Ear30_day_9","RePCM_Bacteria_Conc1","RePCM_Bacteria_Conc2"];
slices = ['7', '6' ,'3' ,'2' ];

%factors_to_include = [1 2 3 4 5 7 8 9 10 11 12 13 16 17:27]; % exp norm and exp reversed as 2 values

%factors_to_include = [1 4 5 7 8 9 10 11 12 13 14 16 17:27]; % exp both as 1 value

%factors_to_include = [1 4 5 7 8 9 10 11 12 13 14 15 16 17:27]; % exp both as 1 value and is normal or reversed

%factors_to_include = [17:27]; % only trajectory

%factors_to_include = [1 2 3 4 5 7 8 9 10 11 12 13 16]; % as previously + depth

factors_to_include = [1 2 3 4 5 13 17:27]; % without cycle analyses values, without depth

%factors_to_include = [1 2 3 4 5 13 16 17:18] ; % only basic

for sample_ind = 1:size(sample_names,2)

    loadname = strcat (loadfolder,sample_names(1,sample_ind),'_wl_',wl,'_slice_',slices(sample_ind),'_features_values.mat');
    load(loadname,  'Factors_values');

    loadname = strcat (loadfolder,sample_names(1,sample_ind),'_wl_',wl,'_slice_',slices(sample_ind),'_classes_labels.mat');
    load(loadname,  'Class_lables');
     
    % balance labels here for each sample
    % (we assume that always less signa)
    number_signal_points = sum(Class_lables == 's');
    number_bg_points = sum(Class_lables == 'b');
    
    if (number_bg_points>number_signal_points)
        [pos_bg] = find(Class_lables == 'b');
        [pos_s] = find(Class_lables == 's');
        rand_ind = randperm(number_bg_points, number_signal_points);
        Factors_values_balanced = [Factors_values(pos_s,factors_to_include);Factors_values(pos_bg(rand_ind),factors_to_include)];
        Class_lables_balanced = [Class_lables(pos_s);Class_lables(pos_bg(rand_ind))];
        params_values = [params_values; Factors_values_balanced];
        data_labels = [data_labels; Class_lables_balanced];
    else 
        
        % if more signal points or equal - keep all
        
        params_values = [params_values; Factors_values(:,factors_to_include)];
        data_labels = [data_labels; Class_lables(:)];
        
    end

end

%%

num_pca = sum(explained>0.05);

params_values_normalized = normalize(params_values,2,'range');

coeff_used = coeff(1:num_pca,:);

params_pc_values = params_values_normalized*coeff_used';

%%
rng(1); % For reproducibility
Mdl = TreeBagger(50,params_pc_values,data_labels,'OOBPrediction','On',...
    'OOBPredictorImportance', 'On', 'Method','classification');
figure;
oobErrorBaggedEnsemble = oobError(Mdl);
plot(oobErrorBaggedEnsemble)
xlabel 'Number of grown trees';
ylabel 'Out-of-bag classification error';

%%

save('Mdl_dt_on_pca_without_depth.mat', 'Mdl' );

