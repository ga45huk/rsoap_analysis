loadfolder = 'H:\2_basic_analysis_values_mat\basic_analysis_unified\'; 

sample_name = '1766_4t1_RePCM_Ear30_day_9';
wl = '770';
slice = '6';

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
load (loadname, 'Anatomy_img')
%%
[roi, background] = get_groundtruth (Anatomy_img);

savefolder = 'H:\2_basic_analysis_values_mat\groundtruth_labels';

savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_groundtruth.mat');
save(savename, 'roi', 'background');

