loadfolder = 'H:\2_basic_analysis_values_mat\ml_models\pca\';

model_name = 'all_without_cycles_analysis_without_depth';

loadname = strcat (loadfolder,model_name,'_pca.mat');
load(loadname, 'params_values_normalized', 'factors_to_include','coeff','score','latent','tsquared','explained','mu');

loadfolder = 'H:\2_basic_analysis_values_mat\features_values_testing\';

params_values = [];

sample_name = '1763_HCT116_RePCM_Ear3_day_11';
slice = '7';

%factors_to_include = [1 2 3 4 5 7 8 9 10 11 12 13 16 17:27]; % exp norm and exp reversed as 2 values

%factors_to_include = [1 4 5 7 8 9 10 11 12 13 14 16 17:27]; % exp both as 1 value

%factors_to_include = [1 4 5 7 8 9 10 11 12 13 14 15 16 17:27]; % exp both as 1 value and is normal or reversed

%factors_to_include = [17:27]; % only trajectory

%factors_to_include = [1 2 3 4 5 7 8 9 10 11 12 13 16]; % as previously + depth

factors_to_include = [1 2 3 4 5 13  17:27]; % without cycle analyses values, without depth

%factors_to_include = [1 2 3 4 5 13 16 17:18] ; % only basic

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_features_values.mat');
load(loadname,  'Factors_values', 'coord');
  
params_values = [params_values; Factors_values(:,factors_to_include)];


num_pca = sum(explained>0.05);

params_values_normalized = normalize(params_values,2,'range');

coeff_used = coeff(1:num_pca,:);

params_pc_values = params_values_normalized*coeff_used';

%%
load ('Mdl_dt_on_pca_without_depth.mat', 'Mdl' );

x_size = max(coord.x);
y_size = max(coord.y);

[prediction_cart_bag,score] = Mdl.predict (params_pc_values);

img = zeros(x_size, y_size, 3);
mask = zeros(x_size, y_size);

score_s_s = 0;
score_bg_s = 0;
score_s_bg = 0;
score_bg_bg = 0;

score_s_map  = zeros(x_size, y_size, 3);
score_b_map  = zeros(x_size, y_size, 3);

points_s = 0;
points_bg = 0;

for i=1:size(prediction_cart_bag)
   if (prediction_cart_bag{i} == 's')
       img(coord.x(i),coord.y(i)) = 1;
       mask(coord.x(i),coord.y(i)) = 1;
       score_s_s = score_s_s + score(i,2);
       score_s_bg = score_s_bg + score(i,1);
       points_s = points_s + 1;
   else
       score_bg_bg = score_bg_bg + score(i,1);
       score_bg_s = score_bg_s + score(i,2);
       points_bg = points_bg + 1;
   end
   score_s_map(coord.x(i),coord.y(i)) = score(i,2);   
   score_b_map(coord.x(i),coord.y(i)) = score(i,1);
end

imshow(img)
