function [exp_fit_normal, exp_fit_reversed, exp_fit_both ] = get_exp_fit (mean_cycle_normalized, Mask_normal, Mask_reversed, pulses_num)

tic
x_size = size(mean_cycle_normalized,1);
y_size = size(mean_cycle_normalized,2);
%pulses_num = size(mean_cycle_normalized,3);

exp_fit_normal.b = zeros (x_size,y_size);
exp_fit_normal.rsqr = zeros (x_size,y_size);

exp_fit_reversed.b = zeros (x_size,y_size);
exp_fit_reversed.rsqr = zeros (x_size,y_size);


exp_normal_traj = @(exp_b,x)(exp(exp_b*(x-1)));
exp_reversed_traj = @(exp_b,x)(-exp(exp_b*(x-1))+1);

fo = fitoptions('Method','NonlinearLeastSquares','StartPoint',-0.3);
fit_normal = fittype(exp_normal_traj,'options',fo);  
fit_reversed = fittype(exp_reversed_traj,'options',fo);  

x = (1:pulses_num)';

for x_ind = 1:x_size
    for y_ind = 1:y_size
        if Mask_reversed (x_ind, y_ind)
           y = squeeze(mean_cycle_normalized(x_ind,y_ind,1:pulses_num));
           [f, gof] = fit(x,y,fit_reversed);  
           exp_fit_reversed.b(x_ind,y_ind) = f.exp_b;
           exp_fit_reversed.rsqr(x_ind,y_ind) = gof.rsquare; 
        end
        if Mask_normal (x_ind, y_ind)         
           y = squeeze(mean_cycle_normalized(x_ind,y_ind,1:pulses_num));
           [f, gof] = fit(x,y,fit_normal);  
           exp_fit_normal.b(x_ind,y_ind) = f.exp_b;
           exp_fit_normal.rsqr(x_ind,y_ind) = gof.rsquare; 
        end
    end
end

exp_fit_both.b = exp_fit_normal.b + exp_fit_reversed.b;
exp_fit_both.rsqr = exp_fit_normal.rsqr + exp_fit_reversed.rsqr;

toc;
disp ('exp fitting');

end

