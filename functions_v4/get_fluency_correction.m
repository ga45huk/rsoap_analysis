function [roi_min_distances, roi_fluency] = get_fluency_correction (Anatomy_img)

initial_image = Anatomy_img;
figure(1); imagesc(initial_image); colormap('gray');
roi = roipoly;
close(figure(1));

B = bwboundaries(roi);
roi_border = B{1};

roi_min_distances = zeros(size(roi)); % 0 if not in roi

[roi_point_x, roi_point_y] = ind2sub(size(roi),find(roi == 1));

smallest_distances = (pdist2(roi_border,[roi_point_x, roi_point_y],'euclidean','Smallest',1));


for point_ind=1:size(roi_point_x,1)
    roi_min_distances (roi_point_x(point_ind),roi_point_y(point_ind)) = smallest_distances(point_ind);
end

exp_points_num = max(roi_min_distances(:))+1;
disp(max(roi_min_distances(:)));
xx = 1:exp_points_num ;
exp_b = - 0.0304;
exp_model = @(x_val)exp(exp_b*(x_val-1));
exp_trace = exp_model(xx');

roi_fluency = zeros(size(roi));

for point_ind=1:size(roi_point_x,1)
    disp(roi_min_distances (roi_point_x(point_ind),roi_point_y(point_ind)));
    roi_fluency(roi_point_x(point_ind),roi_point_y(point_ind)) = exp_trace( floor(roi_min_distances (roi_point_x(point_ind),roi_point_y(point_ind)) + 1));
end
    
subplot(2,1,1); imagesc(roi_min_distances);
subplot(2,1,2); imagesc(roi_fluency);

end