function plot_clusters (data_clustered)
   
   figure('Position',get(0,'ScreenSize')); 
   cluster_num = size(data_clustered.centers,2);
   
   for cluster_ind = 1:cluster_num
       subplot(2,cluster_num,cluster_ind);
       imagesc(squeeze(data_clustered.indexes(:,:,cluster_ind)));
       title(strcat('cluster center ',num2str(data_clustered.centers(cluster_ind))));
       subplot(2,cluster_num,cluster_ind+cluster_num);
       plot(data_clustered.trajectories(:,cluster_ind));
       title ('mean cluster trjectory');
   end
   
   sgtitle (strcat('data clustered by:',data_clustered.custering_type));
end