exp_fit_normal_mix = exp_fit_normal;
%%
exp_fit_normal_prescan = exp_fit_normal;

%%

selection_prescan = (exp_fit_normal_prescan.rsqr > 0.5).*exp_fit_normal_prescan.b;
selection_mix = (exp_fit_normal_mix.rsqr > 0.5).*exp_fit_normal_mix.b;

min_prescan = min(selection_prescan(:));
min_mix = min(selection_mix(:));
min_val = min(min_prescan, min_mix);

subplot(2,2,1);
imagesc(selection_prescan, [-0.5 -0.2]); colorbar;
title('prescan exp const');

subplot(2,2,2);
imagesc(selection_mix, [-0.5 -0.2]); colorbar;
title('mix exp const');

subplot(2,2,3);
imagesc(exp_fit_normal_prescan.rsqr, [0 1]); colorbar;
title('prescan rsqr');

subplot(2,2,4);
imagesc(exp_fit_normal_mix.rsqr, [0 1]); colorbar;
title('mix rsqr');
