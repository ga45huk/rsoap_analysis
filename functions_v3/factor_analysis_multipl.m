rsqr = [];
exp_b_normal = [];
exp_b_reversed = [];
mean_intensities = [];
amplitudes = [];
negative_vals_count = [];
max_pos_median = [];
min_pos_median = [];
max_pos_std = [];
min_pos_std = [];
good_cycles_num = [];
cycle_length = [];
ffts_right_freq = [];

Class_lables_all = [];

%% load data

loadfolder = 'C:\full_basic_analysis\'; 

%loadfolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\'; 
%sample_name = 'Conc_6_Conc_7';

%sample_name = 'mice1_Re4T1_DrJurkat_Switching';
%sample_name = 'Conc1_2_3__1796_Switching';
%sample_name = 'HCT_Scan_2_day_14';
%sample_name = '1763_HCT116_RePCM_Ear3_day_14';

sample_name = 'Rpbac_DrJurkat_ReJurkat_M3';
wl = '770';


slice = '5';

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','recon_name','wl','slice_idx');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
load (loadname, 'basic_analysis_values','ffts');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
load (loadname, 'cycles_analysis_values');


%% get roi

%first select the whole mouse, after - roi
[p_roi, mouse_region] = get_roi (Anatomy_img);

%% get factors values

%only points for which exp fit was estimated 
p_roi_estimated = p_roi.* (exp_fit_both.rsqr~=0);
mouse_region_estimated = mouse_region.* (exp_fit_both.rsqr~=0);


number_points = sum(mouse_region_estimated(:)); %only points for which exp fit was estimated 
x_coord = zeros(number_points, 1);
y_coord = zeros(number_points, 1);

% factors:
% rsqr 
% exp.b
% mean_intensities
% amplitudes
% neative_vals_count
% max_pos_median
% min_pos_median
% max_pos_std
% min_pos_std
% good_cycles_num
% cycle_length

number_factors = 13;
Factors_values = zeros(number_points, number_factors);
Class_lables = char(number_points,1);

x_size = size(Anatomy_img, 1);
y_size = size(Anatomy_img, 2);

p_ind = 0;

for x_ind = 1:x_size
    for y_ind = 1:y_size
        if (p_roi_estimated(x_ind,y_ind))
            p_ind = p_ind + 1;
            x_coord(p_ind) =  x_ind;
            y_coord(p_ind) =  y_ind;
            Factors_values (p_ind, 1) = exp_fit_both.rsqr(x_ind,y_ind);
            Factors_values (p_ind, 2) = exp_fit_normal.b(x_ind,y_ind);
            Factors_values (p_ind, 3) = exp_fit_reversed.b(x_ind,y_ind);
            Factors_values (p_ind, 4) = basic_analysis_values.mean_intensities(x_ind,y_ind);
            Factors_values (p_ind, 5) = basic_analysis_values.amplitudes(x_ind,y_ind);
            Factors_values (p_ind, 6) = basic_analysis_values.negative_vals_count(x_ind,y_ind);
            Factors_values (p_ind, 7) = cycles_analysis_values.max_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 8) = cycles_analysis_values.min_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 9) = cycles_analysis_values.max_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 10) = cycles_analysis_values.min_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 11) = cycles_analysis_values.good_cycles_num(x_ind,y_ind);
            Factors_values (p_ind, 12) = cycles_analysis_values.cycle_length(x_ind,y_ind);
            Factors_values (p_ind, 13) = ffts(x_ind,y_ind);

            Class_lables (p_ind) = 'd'; 
        else
            if (mouse_region_estimated(x_ind,y_ind))
            p_ind = p_ind + 1;
            x_coord(p_ind) =  x_ind;
            y_coord(p_ind) =  y_ind;
            Factors_values (p_ind, 1) = exp_fit_both.rsqr(x_ind,y_ind);
            Factors_values (p_ind, 2) = exp_fit_normal.b(x_ind,y_ind);
            Factors_values (p_ind, 3) = exp_fit_reversed.b(x_ind,y_ind);
            Factors_values (p_ind, 4) = basic_analysis_values.mean_intensities(x_ind,y_ind);
            Factors_values (p_ind, 5) = basic_analysis_values.amplitudes(x_ind,y_ind);
            Factors_values (p_ind, 6) = basic_analysis_values.negative_vals_count(x_ind,y_ind);
            Factors_values (p_ind, 7) = cycles_analysis_values.max_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 8) = cycles_analysis_values.min_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 9) = cycles_analysis_values.max_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 10) = cycles_analysis_values.min_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 11) = cycles_analysis_values.good_cycles_num(x_ind,y_ind);
            Factors_values (p_ind, 12) = cycles_analysis_values.cycle_length(x_ind,y_ind);
            Factors_values (p_ind, 13) = ffts(x_ind,y_ind);

            Class_lables (p_ind) = 'b'; 
            end
        end 
    end
end
%%
save('Factors_values_mouse_3_day_9.mat', 'Factors_values');
%%
save ('Factor_values_test_4t1_mouse3_day_9_slice_6.mat', 'Factors_values', 'x_coord', 'y_coord');
%%
rsqr = [rsqr; Factors_values(:, 1) ];
exp_b_normal = [exp_b_normal; Factors_values(:, 2)];
exp_b_reversed = [exp_b_reversed; Factors_values(:, 3)];
mean_intensities = [mean_intensities; Factors_values(:, 4)];
amplitudes = [amplitudes; Factors_values(:, 5)];
negative_vals_count = [negative_vals_count; Factors_values(:, 6)];
max_pos_median = [max_pos_median; Factors_values(:, 7)];
min_pos_median = [min_pos_median; Factors_values(:, 8)];
max_pos_std = [max_pos_std; Factors_values(:, 9)];
min_pos_std = [min_pos_std; Factors_values(:, 10)];
good_cycles_num = [good_cycles_num; Factors_values(:, 11)];
cycle_length = [cycle_length; Factors_values(:, 12)];
ffts_right_freq = [ffts_right_freq; Factors_values(:, 13)];


Class_lables_all = [Class_lables_all; Class_lables];

FactorTable_test = table(rsqr,exp_b_normal,exp_b_reversed,ffts_right_freq,mean_intensities,amplitudes,negative_vals_count,max_pos_median,min_pos_median,max_pos_std,min_pos_std,good_cycles_num,cycle_length);
%%
save('FactorTable_1_2_m_1_7_9_d.mat', 'FactorTable', 'classErrorDefault_all', 'Class_lables_all');
%%
save('FactorTable_4t1_mouse3_day_1_slice_7_test.mat', 'FactorTable_test');

%%
 plot(classErrorDefault_all);
%% build decision tree based on factor values

tc = fitctree(FactorTable,Class_lables_all,'MaxNumSplits',24,'CrossVal','on');
%%
view(tc.Trained{1},'Mode','graph')
classErrorDefault = kfoldLoss(tc)

%%
tc = fitctree(FactorTable,Class_lables_all);
imp = predictorImportance(tc);
%%
figure;
bar(imp);
title('Predictor Importance Estimates');
ylabel('Estimates');
xlabel('Predictors');
h = gca;
h.XTickLabel = tc.PredictorNames;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';

%%
rng(1); % For reproducibility
Mdl = TreeBagger(100,FactorTable,Class_lables_all,'OOBPrediction','On',...
    'OOBPredictorImportance', 'On', 'Method','classification');
figure;
oobErrorBaggedEnsemble = oobError(Mdl);
plot(oobErrorBaggedEnsemble)
xlabel 'Number of grown trees';
ylabel 'Out-of-bag classification error';
%%
imp = Mdl.OOBPermutedPredictorDeltaError;

figure (2);
bar(imp);
title('Classification Test');
ylabel('Predictor importance estimates');
xlabel('Predictors');
h = gca;
h.XTickLabel = Mdl.PredictorNames;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';
%%

[impGain,predAssociation] = predictorImportance(Mdl2);

%%
impOOB = oobPermutedPredictorImportance(Mdl2);
figure
plot(1:numel(Mdl2.PredictorNames),[impOOB' impGain'])
title('Predictor Importance Estimation Comparison')
xlabel('Predictor variable')
ylabel('Importance')
h = gca;
h.XTickLabel = Mdl2.PredictorNames;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';
legend('OOB permuted','MSE improvement')
grid on
%%

figure
imagesc(predAssociation)
title('Predictor Association Estimates')
colorbar
h = gca;
set(gca,'XTick',[1:13]);
set(gca,'YTick',[1:13]);
h.XTickLabel = Mdl2.PredictorNames;
%h.XTickLabelRotation = 1;
h.TickLabelInterpreter = 'none';
h.YTickLabel = Mdl2.PredictorNames;

%%

prediction_cart_bag = Mdl.predict (FactorTable_test);
[label,score] = Mdl.predict (FactorTable_test);
%prediction_cart_bag = Mdl.predictFcn (FactorTable_test);

img = zeros(x_size, y_size, 3);
mask = zeros(x_size, y_size);

score_s_s = 0;
score_bg_s = 0;
score_s_bg = 0;
score_bg_bg = 0;

score_s_map  = zeros(x_size, y_size, 3);
score_b_map  = zeros(x_size, y_size, 3);
score_d_map  = zeros(x_size, y_size, 3);

points_s = 0;
points_bg = 0;

for i=1:size(prediction_cart_bag)
   if (prediction_cart_bag{i} == 's')
       img(x_coord(i),y_coord(i),1) = 1;
       mask(x_coord(i),y_coord(i)) = 1;
       score_s_s = score_s_s + score(i,2);
 %      score_s_bg = score_s_bg + score(i,3);
       points_s = points_s + 1;
   else
       if (prediction_cart_bag{i} == 'd')
           img(x_coord(i),y_coord(i),2) = 1;
       end
       score_bg_bg = score_bg_bg + score(i,1);
%       score_bg_s = score_bg_s + score(i,3);
       points_bg = points_bg + 1;
   end
%   score_s_map(x_coord(i),y_coord(i)) = score(i,3);   
   score_b_map(x_coord(i),y_coord(i)) = score(i,1);
   score_d_map(x_coord(i),y_coord(i)) = score(i,2);
end

imshow(img)


%%

view(Mdl.Trees{1},'Mode','graph')

%%

im1 = imread('C:\__4_predictors\jurkats_4_conc_1_slice_14.tif');

im2 = imread('C:\__4_predictors\jurkats_4_conc_1_slice_15.tif');

im_all = im1 + im2;

imshow(im_all)


%%
