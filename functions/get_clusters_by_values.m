function data_clustered = get_clusters_by_values (exp_fit_normal, mean_cycle_normalized, rsqr_limit, clusters)

    Data = (exp_fit_normal.rsqr>rsqr_limit).*exp_fit_normal.b;

    data_clustered.indexes = reshape((Data(:)<=(clusters.b_values + clusters.delta)),size(Data,1),size(Data,2),size(clusters.b_values,2)) - reshape((Data(:)<=(clusters.b_values-clusters.delta)),size(Data,1),size(Data,2),size(clusters.b_values,2));
    
    pulses_num = size(mean_cycle_normalized,3);
    all_traj_for_all_clusters = repmat(mean_cycle_normalized,1,1,1,size(clusters.delta,1)) .* permute(repmat(data_clustered.indexes,1,1,1,pulses_num),[1 2 4 3]);
    data_clustered.trajectories = squeeze(sum(all_traj_for_all_clusters,[1,2])./sum((all_traj_for_all_clusters>0),[1,2]));

    data_clustered.centers = clusters.b_values;
    
    data_clustered.custering_type= 'predicted values ';
end
