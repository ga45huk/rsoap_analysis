function plot_cycles_analysis_values (cycles_analysis_values, intensities_concatenated)

current_figure = 1;

figure('Position',get(0,'ScreenSize'));

subplot (3,3,1);
imagesc(cycles_analysis_values.max_pos_median); colorbar;
title ('max position, median value');

subplot (3,3,4);
imagesc(cycles_analysis_values.min_pos_median); colorbar;
title ('min position, median value');

subplot (3,3,2);
imagesc(cycles_analysis_values.max_pos_std); colorbar;
title ('max position, std');

subplot (3,3,5);
imagesc(cycles_analysis_values.min_pos_std); colorbar;
title ('min position, std');

subplot (3,3,3);
imagesc(cycles_analysis_values.max_pos_matching_c_num); colorbar;
title ('number of cycles with right max position');

subplot (3,3,6);
imagesc(cycles_analysis_values.min_pos_matching_c_num); colorbar;
title ('number of cycles with right min position');

    
button_main = 1;
cycles_num = size(cycles_analysis_values.min_pos_matching_c_ind,3);
pulses_num = size(intensities_concatenated,3)/cycles_num;

while (button_main == 1)  %when right mouse button clicked - closed        
   figure (current_figure);
   [x, y, button_main] = ginput(1);
   if (button_main ~= 1) 
       close(current_figure); %when right mouse button clicked - closed
       break;
   end
        
   x = round(x); y = round(y);
   
   % mark point on other subplots
subplot (3,3,1);
imagesc(cycles_analysis_values.max_pos_median); colorbar;
title ('max position, median value');
   
hold on;
plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
hold off;


subplot (3,3,4);
imagesc(cycles_analysis_values.min_pos_median); colorbar;
title ('min position, median value');

   
hold on;
plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
hold off;

subplot (3,3,2);
imagesc(cycles_analysis_values.max_pos_std); colorbar;
title ('max position, std');

   
hold on;
plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
hold off;

subplot (3,3,5);
imagesc(cycles_analysis_values.min_pos_std); colorbar;
title ('min position, std');

   
hold on;
plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
hold off;

subplot (3,3,3);
imagesc(cycles_analysis_values.max_pos_matching_c_num); colorbar;
title ('number of cycles with right max position');
   
hold on;
plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
hold off;

subplot (3,3,6);
imagesc(cycles_analysis_values.min_pos_matching_c_num); colorbar;
title ('number of cycles with right min position');
   
hold on;
plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
hold off;
   
    subplot(3, 1, 3);
    plot (squeeze(intensities_concatenated(y,x,:)));
    hold on;
    xlim( [0 size(intensities_concatenated,3)]) ;
    % mark maximums
    yy = zeros(size(intensities_concatenated,3),1);
    
    pos_max = squeeze(cycles_analysis_values.max_pos(y,x,1,:));
    d_tick = 0:pulses_num:pulses_num*cycles_num-pulses_num;
    pos_max = pos_max + d_tick';
    yy (pos_max) = intensities_concatenated (y,x,pos_max);
    plot (yy,'red');
    xticks(0:pulses_num:pulses_num*cycles_num);
    
    % mark minimums
    yy = zeros(size(intensities_concatenated,3),1);
    
    pos_min = squeeze(cycles_analysis_values.min_pos(y,x,1,:));
    d_tick = 0:pulses_num:cycles_num*pulses_num-pulses_num;
    pos_min = pos_min + d_tick';
    yy (pos_min) = intensities_concatenated (y,x,pos_min);
    plot (yy,'green');
    xticks(0:pulses_num:pulses_num*cycles_num);
    hold off;
    
end

end