function plot_basic_analysis_values (basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img, plot_title)
    
current_figure = 1;

pulses_num = size (trajectories.mean_cycle,3);

%figure('Position',get(0,'ScreenSize'));%figure(current_figure);
sgtitle (plot_title);

subplot (4, 3, 1);
imshow(Anatomy_img, [0 300]);

subplot (4, 3, 2);
imagesc(basic_analysis_values.mean_intensities); colorbar;
title ('mean intensities');

subplot (4, 3, 3);
imagesc(basic_analysis_values.amplitudes); colorbar;
title ('amplitudes');

subplot(4, 3, 4);
imagesc(residuals.normal);  
colorbar;
title (strcat('residuals from expected, normal'));

subplot(4, 3, 5);
imagesc(residuals.reversed);  
colorbar;
title (strcat('residuals from expected, reversed'));

subplot(4, 3, 6);
imagesc(residuals.both);  
colorbar;
title (strcat('residuals from expected, both'));

subplot(4, 3, 7);
imagesc(ffts);
colorbar;
title (strcat('ffts, right frequency'));


button_main = 1;

x_points = 1:pulses_num;

while (button_main == 1)  %when right mouse button clicked - closed        
    figure (current_figure);
    [x, y, button_main] = ginput(1);
    if (button_main ~= 1) 
        close(current_figure); %when right mouse button clicked - closed
        break;
    end
        
    x = round(x); 
    y = round(y);
   
    % mark chosen point on other subplots
   
    subplot (4, 3, 1);
    imshow(Anatomy_img, [0 300]);   
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;


    subplot (4, 3, 2);
    imagesc(basic_analysis_values.mean_intensities); colorbar;
    title ('mean intensities');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;

    subplot (4, 3, 3);
    imagesc(basic_analysis_values.amplitudes); colorbar;
    title ('amplitudes');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;

    subplot(4, 3, 4);
    imagesc(residuals.normal);  
    colorbar;
    title (strcat('residuals from expected, normal'));
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;

    subplot(4, 3, 5);
    imagesc(residuals.reversed);  
    colorbar;
    title (strcat('residuals from expected, reversed'));
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;

    subplot(4, 3, 6);
    imagesc(residuals.both);  
    colorbar;
    title (strcat('residuals from expected, both'));
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;

    subplot(4, 3, 7);
    imagesc(ffts);
    colorbar;
    title (strcat('ffts, right frequency'));
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;


    % plot trajectories
    
    subplot(4, 3, 8);
   
    plot (squeeze(trajectories.mean_cycle (y,x, :)));
    title ('mean cycle');
   
    subplot(4, 3, 9);
        
    y_norm = squeeze(trajectories.mean_cycle_normalized (y,x, :));
    exp_b = - 0.3;     
    exp1 = @(x_val)exp(exp_b*(x_val-1));
    Yfitted = exp1(x_points');
        
    plot (x_points', y_norm, 'r-',x_points', Yfitted, 'b--');
    title ('mean cycle nomalized');        
    legend ( 'normalized', 'expected' );
    
    % plot all cycles concatenated
    subplot(4, 1, 4);
    plot (squeeze(intensities_concatenated(y,x,:)));
    
    
end

    
end