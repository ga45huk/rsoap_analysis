function [residuals, residuals_reversed, residuals_both]= get_residuals (normalized_intensity_cycles,exp_b)
    
    x_size = size (normalized_intensity_cycles,1);
    y_size = size (normalized_intensity_cycles,2);
    pulses_num = size (normalized_intensity_cycles,3);
    
    X = 1:pulses_num;
    exp1 = @(x)(exp(exp_b*(x-1)));
    exp2 = @(x_val)(-exp(exp_b*(x_val-1))+1);
    Y_expected = exp1(X);
    Y_expected2 = exp2(X);
    
    residuals = sum(abs(normalized_intensity_cycles - permute(repmat(Y_expected',1,x_size,y_size),[2 3 1])),3);
    residuals_reversed = sum(abs(normalized_intensity_cycles - permute(repmat(Y_expected2',1,x_size,y_size),[2 3 1])),3);
    
    residuals = residuals./pulses_num;
    residuals_reversed = residuals_reversed./pulses_num;
    residuals_both = min (residuals, residuals_reversed);

end