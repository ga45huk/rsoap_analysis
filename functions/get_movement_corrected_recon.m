function recon_movement_corrected = get_movement_corrected_recon (recon, Anatomy_img, use_non_rigid)
tic
cycles_num = size (recon,3);
pulses_num = size (recon,4);

recon_movement_corrected = zeros (size(recon));

for cycle_ind = 1:cycles_num
    for pulse_ind = 1:pulses_num
        recon_movement_corrected (:,:,cycle_ind,pulse_ind) = get_registered_img (Anatomy_img, recon(:,:,cycle_ind,pulse_ind), use_non_rigid);
    end
end
toc
disp('movement corrected');
end