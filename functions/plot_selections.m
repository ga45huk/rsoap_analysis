function plot_selections(selection)

%figure('Position',get(0,'ScreenSize'));

subplot(2,2,1);
imagesc(selection.rsqr);colorbar;
title('selected rsqr');
subplot(2,2,2);
imagesc(selection.ffts);colorbar;
title('selected fft, right frequency');
subplot(2,2,3);
imagesc(selection.mean_intensities);colorbar;
title('selected mean intensities');
subplot(2,2,4);
imagesc(selection.mean_intensities_fluence_corrected);colorbar;
title('selected mean intensities, fluence corrected');

title_text = strcat('rsqr limit:',num2str(selection.params.rsqr_limit));

if selection.params.include_reversed
    title_text = strcat(title_text,'; reversed included');
else
    title_text = strcat(title_text,'; reversed not included');
end

if (selection.params.good_cycles_num_lim>0)
    title_text =  strcat(title_text,'; minimal number of cycles with trend:',num2str(selection.params.good_cycles_num_lim));
end

sgtitle(title_text);

end