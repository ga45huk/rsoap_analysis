function Anatomy_img = get_anatomy_img (recon)

    ref_img = squeeze (recon(:,:,1,1));
    
    use_non_rigid = 1;
    
    recon_movement_corrected = get_movement_corrected_recon (recon, ref_img, use_non_rigid);

    Anatomy_img = mean(squeeze(recon_movement_corrected),3);
    
end