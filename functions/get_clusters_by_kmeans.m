function data_clustered = get_clusters_by_kmeans (exp_fit_normal, mean_cycle_normalized, rsqr_limit, clusters_num)

    Data = (exp_fit_normal.rsqr>rsqr_limit).*exp_fit_normal.b;
    
    [Cluster_exp, ClCentres_exp] = kmeans(reshape(Data,size(Data,1)*size(Data,2),1),clusters_num+1,'MaxIter',800000);

    cluster_img = reshape (Cluster_exp, size(Data,1), size(Data,2) );

    cl_ind = find(ClCentres_exp);
    
    for i=1:clusters_num
        data_clustered.indexes(:,:,i) = (cluster_img  == cl_ind (i));
    end
    
    pulses_num = size(mean_cycle_normalized,3);
    all_traj_for_all_clusters = repmat(mean_cycle_normalized,1,1,1,clusters_num) .* permute(repmat(data_clustered.indexes,1,1,1,pulses_num),[1 2 4 3]);
    data_clustered.trajectories = squeeze(sum(all_traj_for_all_clusters,[1,2])./sum((all_traj_for_all_clusters>0),[1,2]));

    data_clustered.centers = ClCentres_exp(cl_ind)';
    
    data_clustered.custering_type= 'kmeans ';
end

