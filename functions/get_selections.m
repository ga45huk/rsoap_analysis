function selection = get_selections (mask,rsqr, ffts, mean_intensities,roi_fluency)

mouse_mask = (roi_fluency~=0);
mask.indexes = mask.indexes.*mouse_mask;
selection.rsqr = rsqr.*mask.indexes;
selection.ffts = ffts.*mask.indexes;
selection.mean_intensities = mean_intensities.*mask.indexes - min(mean_intensities(:));
mean_intensities_fluence_corrected = ( mean_intensities- min(mean_intensities(:)) )./roi_fluency;
mean_intensities_fluence_corrected(find(~isfinite(mean_intensities_fluence_corrected))) = 0;
selection.mean_intensities_fluence_corrected = mean_intensities_fluence_corrected.*mask.indexes;


selection.params.rsqr_limit = mask.params.rsqr_limit;
selection.params.good_cycles_num_lim = mask.params.good_cycles_num_lim;
selection.params.include_reversed = mask.params.include_reversed;

end