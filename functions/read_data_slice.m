function Reconstruction_slice = read_data_slice (recon_path, slice_idx, wl_idx)

addpath   ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW');
addpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\_switching_recon_and_analysis_ms');
javaaddpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\MSOTBeans\msotbeans.jar');              
javaaddpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\java_class\xmlbeans-2.5.0\lib\xbean.jar');

datainfo = loadMSOT( recon_path ) ;
rNode = 1; % num of recon
selMat = datainfo.ReconNode(rNode).ReconStructure;
selMat = selMat(:,slice_idx,:,wl_idx,:);
[Reconstruction_slice.Recon,Reconstruction_slice.wl_proc_value, Reconstruction_slice.zpos_proc_value, Reconstruction_slice.ts, Reconstruction_slice.datainfo] = loadMSOTRecon(datainfo,rNode,selMat);
Reconstruction_slice.Recon = squeeze(Reconstruction_slice.Recon);
end
