main_folder_name = 'N:\research\Data\CellEngineering\KM_XTEN\20211203_XTEN_RePCM_Phantom';

scan_num = 1;

rNode = 1;

javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\xbean.jar
javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\msotbeans.jar 

recon_path = strcat(main_folder_name,'\MSOT\','Scan_',num2str(scan_num),'\Scan_',num2str(scan_num),'.msot');
datainfo = loadMSOT( recon_path ) ;
sample_name = datainfo.Name
slices_num = size(strcat(datainfo.ZPositions),2)

%%


image_folder = strcat(main_folder_name,'\Unmixing_Images\');
mat_folder =  strcat(main_folder_name,'\Unmixing_Data\');

mkdir (mat_folder);
mkdir (mat_folder);

wl_ind = 2;
wl = datainfo.Wavelengths(wl_ind);

selMat = datainfo.ReconNode(rNode).ReconStructure;
for slice_idx = 1:slices_num
    selMat_ind = selMat(:,slice_idx,:,wl_ind,:); %last is pulses - 1:11 - if not multiplexing
    [Reconstruction_slice.Recon,Reconstruction_slice.wl_proc_value, Reconstruction_slice.zpos_proc_value, Reconstruction_slice.ts, Reconstruction_slice.datainfo] = loadMSOTRecon(datainfo,rNode,selMat_ind);
    Reconstruction_slice.Recon = squeeze(Reconstruction_slice.Recon);
    [basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Reconstruction_slice.Recon);    
    slice = int2str(slice_idx);
    differential = trajectories.mean_cycle(:,:,1)-trajectories.mean_cycle(:,:,11);
    save_name = strcat(mat_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl),'_basic_analysis.mat');
    save (save_name, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','wl','slice_idx','-v7.3');
end

%%

for slice_index = 1:slices_num
    wl = 770;
    load_name = strcat(mat_folder,sample_name,'_slice',num2str(slice_index),'_wl',num2str(wl),'_basic_analysis.mat');
    load (load_name, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','wl','slice_idx');
    [roi_min_distances, roi_fluency] = get_fluency_correction (Anatomy_img);
    mouse_roi = (roi_fluency~=0);
    limit_for_normal = 0.2;
    limit_for_reversed = 0.2;

    Mask_normal = (residuals.normal < limit_for_normal);%.*mouse_roi;
    Mask_reversed = (residuals.reversed < limit_for_reversed);%.*mouse_roi; 
    pulses_num = size(trajectories.mean_cycle_normalized,3);
    %pulses_num = 11;
    [exp_fit_normal, exp_fit_reversed, exp_fit_both ] = get_exp_fit (trajectories.mean_cycle_normalized, Mask_normal, Mask_reversed, pulses_num);
    
    save_name = strcat(mat_folder,sample_name,'_slice',num2str(slice_index),'_wl',num2str(wl),'_exp_fit.mat');
    save (save_name, 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both', '-v7.3');

    delta.begining = 1; % for cycle first index (from end for reversed ones) 
    delta.end = 1;      % for cycle last index  (from end for reversed ones)%
    intentsity_cycles = reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3));
    cycles_analysis_values = get_cycles_analysis_values (intentsity_cycles, delta);
    save_name = strcat(mat_folder,sample_name,'_slice',num2str(slice_index),'_wl',num2str(wl),'_cycles_analysis_values.mat');
    save (save_name, 'cycles_analysis_values','sample_name','wl','slice_idx','-v7.3');

end

%%

clear volume
clear volume_Anatomy
clear volume_rsqr
clear volume_kinetics
clear volume_amplitudes
clear volume_mean_intensities

load ('Mdl1_3.mat','Mdl');

wl = '770';
loadfolder = mat_folder; 
for slice_idx = 1:slices_num
    
slice = int2str(slice_idx);
loadname = strcat(mat_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl),'_exp_fit.mat');
load (loadname, 'exp_fit_normal','exp_fit_reversed', 'exp_fit_both');

loadname = strcat(mat_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl),'_basic_analysis.mat');
load (loadname, 'basic_analysis_values','ffts','Anatomy_img');

loadname = strcat(mat_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl),'_cycles_analysis_values.mat');
load (loadname, 'cycles_analysis_values');


mouse_region = ones(size(Anatomy_img));

mouse_region_estimated = mouse_region.* (exp_fit_both.rsqr~=0);


number_points = sum(mouse_region_estimated(:)); %only points for which exp fit was estimated 
x_coord = zeros(number_points, 1);
y_coord = zeros(number_points, 1);

% factors:
% rsqr 
% exp.b
% mean_intensities
% amplitudes
% neative_vals_count
% max_pos_median
% min_pos_median
% max_pos_std
% min_pos_std
% good_cycles_num
% cycle_length

number_factors = 13;
Factors_values = zeros(number_points, number_factors);

x_size = size(Anatomy_img, 1);
y_size = size(Anatomy_img, 2);

p_ind = 0;

for x_ind = 1:x_size
    for y_ind = 1:y_size

       if (mouse_region_estimated(x_ind,y_ind))
            p_ind = p_ind + 1;
            x_coord(p_ind) =  x_ind;
            y_coord(p_ind) =  y_ind;
            Factors_values (p_ind, 1) = exp_fit_both.rsqr(x_ind,y_ind);
            Factors_values (p_ind, 2) = exp_fit_normal.b(x_ind,y_ind);
            Factors_values (p_ind, 3) = exp_fit_reversed.b(x_ind,y_ind);
            Factors_values (p_ind, 4) = basic_analysis_values.mean_intensities(x_ind,y_ind);
            Factors_values (p_ind, 5) = basic_analysis_values.amplitudes(x_ind,y_ind);
            Factors_values (p_ind, 6) = basic_analysis_values.negative_vals_count(x_ind,y_ind);
            Factors_values (p_ind, 7) = cycles_analysis_values.max_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 8) = cycles_analysis_values.min_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 9) = cycles_analysis_values.max_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 10) = cycles_analysis_values.min_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 11) = cycles_analysis_values.good_cycles_num(x_ind,y_ind);
            Factors_values (p_ind, 12) = cycles_analysis_values.cycle_length(x_ind,y_ind);
            Factors_values (p_ind, 13) = ffts(x_ind,y_ind);

        end 
    end
end

rsqr = Factors_values(:, 1) ;
exp_b_normal = Factors_values(:, 2) ;
exp_b_reversed =  Factors_values(:, 3) ;
mean_intensities = Factors_values(:, 4) ;
amplitudes = Factors_values(:, 5) ;
negative_vals_count = Factors_values(:, 6) ;
max_pos_median = Factors_values(:, 7) ;
min_pos_median = Factors_values(:, 8) ;
max_pos_std = Factors_values(:, 9) ;
min_pos_std = Factors_values(:, 10) ;
good_cycles_num = Factors_values(:, 11) ;
cycle_length = Factors_values(:, 12) ;
ffts_right_freq = Factors_values(:, 13) ;

FactorTable_test = table(rsqr,exp_b_normal,exp_b_reversed,ffts_right_freq,mean_intensities,amplitudes,negative_vals_count,max_pos_median,min_pos_median,max_pos_std,min_pos_std,good_cycles_num,cycle_length);


prediction_cart_bag = Mdl.predict (FactorTable_test);

img = zeros(x_size, y_size, 3);

for i=1:size(prediction_cart_bag)
   if (prediction_cart_bag{i} == 's')
       img(x_coord(i),y_coord(i)) = 1;
   end
end


volume(:,:,slice_idx,:) = img;
%imshow(img)
volume_Anatomy(:,:,slice_idx,:) = Anatomy_img;
imshow(Anatomy_img)
volume_rsqr (:,:,slice_idx,:) = mouse_region_estimated.*exp_fit_both.rsqr;
volume_kinetics (:,:,slice_idx,:) = mouse_region_estimated.*exp_fit_both.b;

volume_amplitudes (:,:,slice_idx,:) = mouse_region_estimated.*basic_analysis_values.amplitudes;
volume_mean_intensities (:,:,slice_idx,:) = mouse_region_estimated.*basic_analysis_values.mean_intensities;

end

%%
for slice_idx = 1:slices_num
    
   slice = int2str(slice_idx);

    current_sample_name = strcat (image_folder,sample_name,'_slice_',slice);
    
    mask =squeeze(volume(:,:,slice_idx,1));

Anatomy_img = volume_Anatomy(:,:,slice_idx,:);
    I = Anatomy_img - min(Anatomy_img(:));
    I = I ./ max(I(:));
    Anatomy_img_scaled = imresize(I,[900 900]);

    RGB = zeros(size(Anatomy_img));



    [x_pos y_pos]   = find(mask);


    pos = [y_pos x_pos];
    RGB = insertMarker(RGB,pos,'o','color','red','size',1);


cluster_img = imresize(RGB,[900, 900]);

backgroundcolor = [0 0 0];

delta_color = 0;

for x_ind=1:900
    for y_ind = 1:900
        if sum(abs(squeeze(cluster_img(x_ind,y_ind,:) - backgroundcolor)))<delta_color
            cluster_img(x_ind,y_ind,:) = backgroundcolor;
        end
    end
end



img_unmix = cluster_img;
img_anatomy = Anatomy_img_scaled;

imwrite(img_unmix,strcat(current_sample_name,'_clusters.png') );


img_anatomy_cutted = cat(3, img_anatomy, img_anatomy, img_anatomy);

img_anatomy_cutted (img_unmix~=0) = 0;

img_overlay = img_unmix + img_anatomy_cutted;

figure(slice_idx);
imshow(img_overlay);
imwrite(img_overlay,strcat(current_sample_name,'_overlayed_clusters.png') );



end