%% get selection from already processed data

%% load data
loadfolder = 'C:\full_basic_analysis\'; 
sample_name = '1766_4t1_RePCM_Ear30_day_9';
wl = '770';
slice = '6';

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_both','trajectories','recon_name','wl','slice_idx');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
load (loadname, 'basic_analysis_values','ffts');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
load (loadname, 'cycles_analysis_values');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_fluency_correction.mat');
load (loadname, 'roi_min_distances', 'roi_fluency');



%% get selections

rsqr_limit = 0.5;        % R-square for exp fitting quality
include_reversed = 0;    % 1 if included 
good_cycles_num_lim = 0; % 0 if not limited


mask = get_selection_mask (rsqr_limit,good_cycles_num_lim,include_reversed,exp_fit_normal,exp_fit_both,cycles_analysis_values);


selection = get_selections (mask,exp_fit_both.rsqr, ffts, basic_analysis_values.mean_intensities,roi_fluency);

plot_selections(selection);

%% save selection
%sample_name = '1766_4t1_RePCM_Ear30_day_1';

%savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_selection.mat');
%save (savename, 'selection','sample_name','recon_name','wl','slice_idx','-v7.3');

