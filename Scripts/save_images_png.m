%% save Anatomy as .png

I = Anatomy_img - min(Anatomy_img(:));
I = I ./ max(I(:));
Anatomy_img_scaled = imresize(I,[900 900]);
imshow(Anatomy_img_scaled);

imwrite(Anatomy_img_scaled, strcat(sample_name,'_anatomy.png'));

%% save rsqr as png
max_rsqr = 0.9848;

TransparencyData = mask.*exp_fit_both.rsqr; 
%TransparencyData = max (max(rsqr_19 .* mask_19,rsqr_18 .* mask_18), mask_20.*rsqr_20);

%TransparencyData = max(max(rsqr1 .* mask1,rsqr2 .* mask2),rsqr3 .* mask3) ;

%TransparencyData = unmixing;

TransparencyData (1,1) = max_rsqr;

unmix_img = ind2rgb(im2uint8((TransparencyData)), parula(256));

backgroundcolor = ind2rgb(im2uint8(mat2gray(0)), parula(256));

unmix_img (1,1,:) = backgroundcolor;

delta_color = 0.01; 

unmix_img_scaled = imresize(unmix_img,[900 900]);

for x_ind=1:900
    for y_ind = 1:900
        if sum(squeeze(unmix_img_scaled(x_ind,y_ind,:) - backgroundcolor))<delta_color
            unmix_img_scaled(x_ind,y_ind,:) = backgroundcolor;
        end
    end
end

imshow(unmix_img_scaled);

imwrite(unmix_img_scaled,strcat(sample_name,'_rsqr.png'),'Transparency', ind2rgb(im2uint8(mat2gray(0)), parula(256)) );

%% save kinetics as png
%max_b = 0.35;

%TransparencyData = mask.indexes.*(roi_fluency>0).*exp_fit_both.b*-1; 

TransparencyData = (mask.*exp_fit_both.b*-1);%.*(exp_fit_both.rsqr>0.9);%.*(roi_fluency>0); 


%TransparencyData (1,1) = max_b;

%TransparencyData(TransparencyData > max_b) = max_b;

unmix_img = ind2rgb(im2uint8((TransparencyData)), parula(110));

backgroundcolor = ind2rgb(im2uint8(mat2gray(0)), parula(256));

unmix_img (1,1,:) = backgroundcolor;

delta_color = 0.2; 

unmix_img_scaled = imresize(unmix_img,[900 900]);

for x_ind=1:900
    for y_ind = 1:900
        if sum(squeeze(unmix_img_scaled(x_ind,y_ind,:) - backgroundcolor))<delta_color
            unmix_img_scaled(x_ind,y_ind,:) = backgroundcolor;
        end
    end
end

imshow(unmix_img_scaled);

imwrite(unmix_img_scaled,strcat(sample_name,'_kinetics.png'),'Transparency', ind2rgb(im2uint8(mat2gray(0)), parula(256)) );

%% mark selection image

RGB = zeros(size(Anatomy_img));

[x_pos y_pos]   = find(mask.*mouse_region == 1);

%[x_pos y_pos]   = find(mask == 1);

pos = [y_pos x_pos];
RGB = insertMarker(RGB,pos,'o','color','blue','size',1);

%[x_pos y_pos]   = find(reg == 1);

%pos = [y_pos x_pos];
%RGB = insertMarker(RGB,pos,'o','color','red','size',1);


cluster_img = imresize(RGB,[900, 900]);

backgroundcolor = [0 0 0];

delta_color = 0.1;

for x_ind=1:900
    for y_ind = 1:900
        if sum(abs(squeeze(cluster_img(x_ind,y_ind,:) - backgroundcolor)))<delta_color
            cluster_img(x_ind,y_ind,:) = backgroundcolor;
        end
    end
end


imshow(cluster_img);

imwrite(cluster_img,strcat(sample_name,'_clusters.png'),'Transparency', [0 0 0] );

%%
%% save overlay as png
img_unmix = imread(strcat(sample_name,'_rsqr.png'));
img_anatomy = imread(strcat(sample_name,'_anatomy.png'));

img_anatomy_cutted = cat(3, img_anatomy, img_anatomy, img_anatomy);

img_anatomy_cutted (img_unmix~=0) = 0;
save
img_overlay = img_unmix + img_anatomy_cutted;
imshow(img_overlay);
imwrite(img_overlay,strcat(sample_name,'_overlayed_rsqr.png') );
%% save overlay as png

img_unmix = imread(strcat(sample_name,'_kinetics.png'));
img_anatomy = imread(strcat(sample_name,'_anatomy.png'));

img_anatomy_cutted = cat(3, img_anatomy, img_anatomy, img_anatomy);

img_anatomy_cutted (img_unmix~=0) = 0;

img_overlay = img_unmix + img_anatomy_cutted;
imshow(img_overlay);
imwrite(img_overlay,strcat(sample_name,'_overlayed_kinetics.png') );

%% save overlay as png

img_unmix = imread(strcat(sample_name,'_clusters.png'));
img_anatomy = imread(strcat(sample_name,'_anatomy.png'));

img_anatomy_cutted = cat(3, img_anatomy, img_anatomy, img_anatomy);

img_anatomy_cutted (img_unmix~=0) = 0;

img_overlay = img_unmix + img_anatomy_cutted;
imshow(img_overlay);
imwrite(img_overlay,strcat(sample_name,'_overlayed_clusters.png') );

%%

img1 = imread(strcat(sample_name,'slice_1_rsqr.png'));
img2 = imread(strcat(sample_name,'slice_2_rsqr.png'));
img3 = imread(strcat(sample_name,'slice_3_rsqr.png'));

%img1 (img2~=0) = 0;

img_overlay = img1;

imshow(img_overlay);


imwrite(img_overlay,strcat(sample_name,'_rsqr.png') );
