%% load ml model

load ('Mdl1_3.mat','Mdl'); 

%% load data
loadfolder = 'C:\full_basic_analysis\'; 

sample_name = 'Jurkats_old_Conc_2';
wl = '770';
slice = '4';

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','recon_name','wl','slice_idx');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
load (loadname, 'basic_analysis_values','ffts');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
load (loadname, 'cycles_analysis_values');


%% get roi

%first select the whole mouse, after - roi
[roi.p_roi, roi.mouse_region] = get_roi (Anatomy_img);

%% get factors values

Factors_values = get_factors_values (roi, exp_fit_both, exp_fit_normal, exp_fit_reversed, ffts, basic_analysis_values, cycles_analysis_values);

rsqr = Factors_values(:, 1) ;
exp_b_normal = Factors_values(:, 2);
exp_b_reversed = Factors_values(:, 3);
mean_intensities = Factors_values(:, 4);
amplitudes = Factors_values(:, 5);
negative_vals_count = Factors_values(:, 6);
max_pos_median = Factors_values(:, 7);
min_pos_median = Factors_values(:, 8);
max_pos_std = Factors_values(:, 9);
min_pos_std = Factors_values(:, 10);
good_cycles_num = Factors_values(:, 11);
cycle_length = Factors_values(:, 12);
ffts_right_freq = Factors_values(:, 13);

FactorTable_test = table(rsqr,exp_b_normal,exp_b_reversed,ffts_right_freq,mean_intensities,amplitudes,negative_vals_count,max_pos_median,min_pos_median,max_pos_std,min_pos_std,good_cycles_num,cycle_length);

%% perform prediction on data

prediction_cart_bag = Mdl.predict (FactorTable_test);
[label,score] = Mdl.predict (FactorTable_test);

img = zeros(x_size, y_size, 3);
mask = zeros(x_size, y_size);

score_s_s = 0;
score_bg_s = 0;
score_s_bg = 0;
score_bg_bg = 0;

score_s_map  = zeros(x_size, y_size, 3);
score_b_map  = zeros(x_size, y_size, 3);

points_s = 0;
points_bg = 0;

for i=1:size(prediction_cart_bag)
   if (prediction_cart_bag{i} == 's')
       img(x_coord(i),y_coord(i)) = 1;
       mask(x_coord(i),y_coord(i)) = 1;
       score_s_s = score_s_s + score(i,2);
       score_s_bg = score_s_bg + score(i,1);
       points_s = points_s + 1;
   else
       score_bg_bg = score_bg_bg + score(i,1);
       score_bg_s = score_bg_s + score(i,2);
       points_bg = points_bg + 1;
   end
   score_s_map(x_coord(i),y_coord(i)) = score(i,2);   
   score_b_map(x_coord(i),y_coord(i)) = score(i,1);
end

figure(1);
imshow(img);
title ('unmixing result');

figure(2);
imshow(score_s_map);
title ('signal scores');

figure(3);
imshow(score_b_map);
title ('background scores');
