%% get clusters script 

%% load data
loadfolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';
sample_name = 'triple_bac_040719_scan_3';
wl = '770';
slice = '1';

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
load (loadname, 'basic_analysis_values','ffts', 'trajectories');


%% clustering based on expected values

rsqr_limit = 0.5;
clusters.b_values = [-0.25 -0.12 -0.05];
clusters.delta = 0.02;

data_clustered = get_clusters_by_values (exp_fit_normal, trajectories.mean_cycle_normalized, rsqr_limit, clusters);

plot_clusters(data_clustered);

%% clustering by kmeans
clusters_num = 3;
data_clustered = get_clusters_by_kmeans (exp_fit_normal, trajectories.mean_cycle_normalized, rsqr_limit, clusters_num);
plot_clusters(data_clustered);


%% save clustering results

savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_clusters.mat');
save (savename, 'data_clustered','sample_name','recon_name','wl','slice_idx','-v7.3');

