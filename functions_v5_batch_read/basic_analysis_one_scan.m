function basic_analysis_one_scan (path,scan_num,recon_node,roi_selection)

%addpath   ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW');
%addpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\_switching_recon_and_analysis_ms');
%javaaddpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\MSOTBeans\msotbeans.jar');              
%javaaddpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\java_class\xmlbeans-2.5.0\lib\xbean.jar');
%addpath   ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691');
%addpath   ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691\classes');
%javaaddpath ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691\MSOTBeans\msotbeans.jar');              
%javaaddpath ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691\MSOTBeans\xbean.jar');
%addpath   ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW');
%addpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\_switching_recon_and_analysis_ms');


javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\xbean.jar
javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\msotbeans.jar 

%%
%main_folder_name = 'N:\research\Data\CellEngineering\1804_KM_Switching\KM_Phantom_090720';
main_folder_name = path;



recon_path = strcat(main_folder_name,'\MSOT\','Scan_',num2str(scan_num),'\Scan_',num2str(scan_num),'.msot');
datainfo = loadMSOT( recon_path ) ;
sample_name = datainfo.Name
%% remove later!
%sample_name = 'XTEN_RePCM_1'
%'47_210603_Conc1_position1_sample1_2'

images_folder = strcat(main_folder_name,'\Unmixing_Images\');
mat_folder = strcat(main_folder_name,'\Unmixing_Data\');

mkdir (mat_folder);
mkdir (images_folder);

slices_num = size(strcat(datainfo.ZPositions),2)

rNode = recon_node;
wl_ind = 2;
wl = datainfo.Wavelengths(wl_ind);

selMat = datainfo.ReconNode(rNode).ReconStructure;


for slice_idx = 1:slices_num
    selMat_ind = selMat(:,slice_idx,:,wl_ind,:); %last is pulses - 1:11 - if not multiplexing
    [Reconstruction_slice.Recon,Reconstruction_slice.wl_proc_value, Reconstruction_slice.zpos_proc_value, Reconstruction_slice.ts, Reconstruction_slice.datainfo] = loadMSOTRecon(datainfo,rNode,selMat_ind);
    Reconstruction_slice.Recon = squeeze(Reconstruction_slice.Recon);
    [basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Reconstruction_slice.Recon);    
    slice = int2str(slice_idx);

    figure(slice_idx);
    imagesc (residuals.normal); colorbar;
    
    figure(slice_idx*10);
    imagesc(ffts);colorbar;
    
    differential = trajectories.mean_cycle(:,:,1)-trajectories.mean_cycle(:,:,11);

    figure(slice_idx*100);
    imagesc(differential);colorbar;
    
    
    save_name = strcat(images_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl));

    I = Anatomy_img - min(Anatomy_img(:));
    I = I ./ max(I(:));
    Anatomy_img_scaled = imresize(I,[900 900]);
    imwrite(Anatomy_img_scaled, strcat(save_name,'_anatomy.png'));
    
    imwrite(ind2rgb(im2uint8(residuals.normal/max(residuals.normal(:))), parula(250)), strcat(save_name,'_residuals.png'))
    imwrite(ind2rgb(im2uint8(ffts/max(ffts(:))), parula(250)), strcat(save_name,'_ffts.png'))
    imwrite(ind2rgb(im2uint8(differential/max(differential(:))), parula(250)), strcat(save_name,'_differential.png'))

    
   save_name = strcat(mat_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl),'_basic_analysis.mat');
   save (save_name, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','wl','slice_idx','-v7.3');

 

end


 

%addpath   ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW');
%addpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\_switching_recon_and_analysis_ms');
%javaaddpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\MSOTBeans\msotbeans.jar');              
%javaaddpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\java_class\xmlbeans-2.5.0\lib\xbean.jar');
%addpath   ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691');
%addpath   ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691\classes');
%javaaddpath ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691\MSOTBeans\msotbeans.jar');              
%javaaddpath ('N:\CellEngineering\software\MATLAB\MSOT_LIBS\com.itheramedical.msotlib_beta_rev691\MSOTBeans\xbean.jar');
%addpath   ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW');
%addpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\_switching_recon_and_analysis_ms');


javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\xbean.jar
javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\msotbeans.jar 

%%
%main_folder_name = 'N:\research\Data\CellEngineering\1804_KM_Switching\KM_Phantom_090720';
main_folder_name = 'N:\research\Data\CellEngineering\Yishu\tests\PhantomTest250221';
scan_num = 29;



recon_path = strcat(main_folder_name,'\MSOT\','Scan_',num2str(scan_num),'\Scan_',num2str(scan_num),'.msot');
datainfo = loadMSOT( recon_path ) ;
sample_name = datainfo.Name
%% remove later!
%sample_name = 'XTEN_RePCM_1'
%'47_210603_Conc1_position1_sample1_2'

images_folder = strcat(main_folder_name,'\Unmixing_Images\');
mat_folder = strcat(main_folder_name,'\Unmixing_Data\');

mkdir (mat_folder);
mkdir (images_folder);

slices_num = size(strcat(datainfo.ZPositions),2)

rNode = 1;
wl_ind = 2;
wl = datainfo.Wavelengths(wl_ind);

selMat = datainfo.ReconNode(rNode).ReconStructure;


for slice_idx = 1:slices_num
    selMat_ind = selMat(:,slice_idx,:,wl_ind,:); %last is pulses - 1:11 - if not multiplexing
    [Reconstruction_slice.Recon,Reconstruction_slice.wl_proc_value, Reconstruction_slice.zpos_proc_value, Reconstruction_slice.ts, Reconstruction_slice.datainfo] = loadMSOTRecon(datainfo,rNode,selMat_ind);
    Reconstruction_slice.Recon = squeeze(Reconstruction_slice.Recon);
    [basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Reconstruction_slice.Recon);    
    slice = int2str(slice_idx);

    figure(slice_idx);
    imagesc (residuals.normal); colorbar;
    
    figure(slice_idx*10);
    imagesc(ffts);colorbar;
    
    differential = trajectories.mean_cycle(:,:,1)-trajectories.mean_cycle(:,:,11);

    figure(slice_idx*100);
    imagesc(differential);colorbar;
    
    
    save_name = strcat(images_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl));

    I = Anatomy_img - min(Anatomy_img(:));
    I = I ./ max(I(:));
    Anatomy_img_scaled = imresize(I,[900 900]);
    imwrite(Anatomy_img_scaled, strcat(save_name,'_anatomy.png'));
    
    imwrite(ind2rgb(im2uint8(residuals.normal/max(residuals.normal(:))), parula(250)), strcat(save_name,'_residuals.png'))
    imwrite(ind2rgb(im2uint8(ffts/max(ffts(:))), parula(250)), strcat(save_name,'_ffts.png'))
    imwrite(ind2rgb(im2uint8(differential/max(differential(:))), parula(250)), strcat(save_name,'_differential.png'))

    
   save_name = strcat(mat_folder,sample_name,'_slice',num2str(slice_idx),'_wl',num2str(wl),'_basic_analysis.mat');
   save (save_name, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','wl','slice_idx','-v7.3');

 

end



