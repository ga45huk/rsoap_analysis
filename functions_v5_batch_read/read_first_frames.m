function current_roi = read_first_frames (path,scan_num,recon_node)

    main_folder_name = path;


    javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\xbean.jar
    javaaddpath H:\full_analysis\com.itheramedical.msotlib_rev723\MSOTBeans\msotbeans.jar 

    recon_path = strcat(main_folder_name,'\MSOT\','Scan_',num2str(scan_num),'\Scan_',num2str(scan_num),'.msot');
    datainfo = loadMSOT( recon_path ) ;
    
    slices_num = size(strcat(datainfo.ZPositions),2);
    
    selMat = datainfo.ReconNode(recon_node).ReconStructure;
    
    wl_ind = 2;
    for slice_idx = 1:slices_num
            selMat_ind = selMat(1,slice_idx,1,wl_ind,1); %last is pulses - 1:11 - if not multiplexing
            [Reconstruction_slice.Recon,Reconstruction_slice.wl_proc_value, Reconstruction_slice.zpos_proc_value, Reconstruction_slice.ts, Reconstruction_slice.datainfo] = loadMSOTRecon(datainfo,rNode,selMat_ind);
            Reconstruction_slice.Recon = squeeze(Reconstruction_slice.Recon);
            
            current_roi(i) = choose_roi (squeeze(Reconstruction_slice.Recon(:,:,1,1)));
    end
    
end
    